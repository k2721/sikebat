<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kewarganegaraan extends Model
{
    public $table = "tb_kewarganegaraan";
    protected $primaryKey = 'id_kewarganegaraan';
    public $timestamps = false;

    protected $fillable = [
        'id_kewarganegaraan', 'kewarganegaraan'
    ];
}
