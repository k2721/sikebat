<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Aktakelahiran extends Model
{
    public $table = "tb_akta_kelahiran";
    protected $primaryKey = 'id_akta_kelahiran';
    public $timestamps = false;

    protected $fillable = [
        'id_akta_kelahiran', 'nik', 'nama_anak', 'nama_ayah', 'nama_ibu', 'tempat_lahir', 'tanggal_lahir', 'id_jeniskelamin'
    ];

}