<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Agama extends Model
{
    public $table = "tb_agama";
    protected $primaryKey = 'id_agama';
    public $timestamps = false;

    protected $fillable = [
        'id_agama', 'nama_agama'
    ];
}