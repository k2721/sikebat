<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Golongandarah extends Model
{
    public $table = "tb_golongandarah";
    protected $primaryKey = 'id_golongandarah';
    public $timestamps = false;

    protected $fillable = [
        'id_golongandarah', 'golongan_darah'
    ];
}