<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pengumuman extends Model
{
    public $table = "tb_pengumuman";
    protected $primaryKey = 'id_pengumuman';
    public $timestamps = false;

    protected $fillable = [
        'id_pengumuman', 'nama_kegiatan', 'tanggal_pelaksanaan', 'informasi_pelaksanaan', 'gambar'
    ];

}