<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Aktakematian extends Model
{
    public $table = "tb_akte_kematian";
    protected $primaryKey = 'id_akta_kematian';
    public $timestamps = false;

    protected $fillable = [
        'id_akta_kematian', 'nik', 'nama', 'tempat_lahir', 'tanggal_lahir', 'tempat_kematian', 'tanggal_kematian', 'id_jeniskelamin', 'anak_ke', 'id_agama'
    ];

}