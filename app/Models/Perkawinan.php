<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Perkawinan extends Model
{
    public $table = "tb_perkawinan";
    protected $primaryKey = 'id_perkawinan';
    public $timestamps = false;

    protected $fillable = [
        'id_perkawinan', 'status_perkawinan'
    ];
}