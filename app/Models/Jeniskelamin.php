<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jeniskelamin extends Model
{
    public $table = "tb_jeniskelamin";
    protected $primaryKey = 'id_jeniskelamin';
    public $timestamps = false;

    protected $fillable = [
        'id_jeniskelamin', 'jenis_kelamin'
    ];
}
