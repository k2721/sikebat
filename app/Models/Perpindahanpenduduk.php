<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Perpindahanpenduduk extends Model
{
    public $table = "tb_pindah";
    protected $primaryKey = 'id_pindah';
    public $timestamps = false;

    protected $fillable = [
        'id_pindah', 'nik', 'no_kk', 'alamat', 'rt', 'rw', 'kelurahan', 'kecamatan', 'kota'
    ];

}