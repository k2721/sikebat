<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kartukeluarga extends Model
{
    public $table = "tb_kk";
    protected $primaryKey = 'no_kk';
    public $timestamps = false;

    protected $fillable = [
        'no_kk', 'nik', 'alamat', 'kelurahan', 'kota', 'kode_pos', 'provinsi', 'rt', 'rw'
    ];

}