<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Penduduk extends Model
{
    public $table = "tb_penduduk";
    protected $primaryKey = 'nik';
    public $timestamps = false;

    protected $fillable = [
        'nik', 'nama', 'tempat_lahir', 'tanggal_lahir', 'id_jeniskelamin', 'id_golongandarah', 'pekerjaan', 'pendidikan', 'id_perkawinan', 'id_kewarganegaraan', 'id_agama', 'no_kk'
    ];

    public function euro()
   {
      return $this->belongsTo('App\Models\Jeniskelamin');
   }
}
