<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    public $table = "tb_pengaduan";
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
         'id', 'nama', 'kritikSaran'
    ];

}
