<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;
use App\Models\Jeniskelamin;
use App\Models\Golongandarah;
use App\Models\Perkawinan;
use App\Models\Agama;
use App\Models\Kewarganegaraan;

class PendataanPendudukController extends Controller
{
    public function index()
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $kewarganegaraans = DB::table('tb_kewarganegaraan')->get();
        $golongandarahs = DB::table('tb_golongandarah')->get();
        $perkawinans = DB::table('tb_perkawinan')->get();
        $agamas = DB::table('tb_agama')->get();
        return view ('pendataanpenduduk', compact('jeniskelamins','kewarganegaraans', 'golongandarahs', 'perkawinans', 'agamas'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'golongan_darah' => 'required',
            'pekerjaan' => 'required',
            'pendidikan' => 'required',
            'status_perkawinan' => 'required',
            'kewarganegaraan' => 'required',
            'usia' => 'required'
        ]);

        $penduduk = Penduduk::create([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'jenis_kelamin' => $request->jenis_kelamin,
        'golongan_darah' => $request->golongan_darah,
        'pekerjaan' => $request->pekerjaan,
        'pendidikan' => $request->pendidikan,
        'status_perkawinan' => $request->status_perkawinan,
        'kewarganegaraan' => $request->kewarganegaraan,
        'id_agama' => $request->id_agama,
        'usia' => $request->usia,
        'no_kk' => $request->no_kk
        ]);

        if($penduduk){
        //redirect dengan pesan sukses
            return redirect()->route('pendataanpenduduk')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('pendataanpenduduk')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }
}
