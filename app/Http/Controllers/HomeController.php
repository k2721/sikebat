<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengumuman;
use App\Models\Penduduk;
use App\Models\Kartukeluarga;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $penduduks= Penduduk::get();
        $pengumumans =Pengumuman::get();
        $kartukeluargas = DB::table('tb_kk')->get();
        $lakis = DB::table('tb_penduduk')->where('id_jeniskelamin', '=', '1',$request->id_jeniskelamin)->get();
        $perempuans = DB::table('tb_penduduk')->where('id_jeniskelamin', '=', '2',$request->id_jeniskelamin)->get();

        return view('home',compact('penduduks', 'pengumumans','kartukeluargas','lakis','perempuans'));
    }
}
