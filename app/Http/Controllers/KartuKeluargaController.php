<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kartukeluarga;
use Illuminate\Support\Facades\DB;

class KartuKeluargaController extends Controller
{
    public function index()
    {
        return view ('kartukeluarga');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'no_kk' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'kelurahan' => 'required',
            'kota' => 'required',
            'kode_pos' => 'required',
            'provinsi' => 'required',
            'rt' => 'required',
            'rw' => 'required'
        ]);

        $kartukeluarga = Kartukeluarga::create([
            'no_kk' => $request->no_kk,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kota' => $request->kota,
            'kode_pos' => $request->kode_pos,
            'provinsi' => $request->provinsi,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kewarganegaraan' => $request->kewarganegaraan,
            'id_agama' => $request->id_agama,
            'usia' => $request->usia,
            'no_kk' => $request->no_kk
            ]);

        if($kartukeluarga){
        //redirect dengan pesan sukses
            return redirect()->route('kartukeluargas')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('kartukeluargas')->with(['error' => 'Data Gagal Disimpan!']);
        }
        
    }
}