<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aktakelahiran;
use App\Models\Jeniskelamin;
use App\Models\Agama;

class KelahiranController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        $aktakelahirans = DB::table('tb_akta_kelahiran')->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        $q = DB::table('tb_akta_kelahiran')->select(DB::raw('MAX(RIGHT(id_akta_kelahiran,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('kelahiran', compact('aktakelahirans', 'jeniskelamins', 'agamas', 'kd'));
    }
    public function store(Request $request)
    {
        $this->validate(request(), [

            'nik' => 'required|min:16|max:16',
            'nama_anak' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'id_jeniskelamin' => 'required'
        ]);

        $aktakelahiran = Aktakelahiran::create([
        'nik' => $request->nik,
        'nama_anak' => $request->nama_anak,
        'nama_ayah' => $request->nama_ayah,
        'nama_ibu' => $request->nama_ibu,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'id_jeniskelamin' => $request->id_jeniskelamin
        ]);

        if($aktakelahiran){
        //redirect dengan pesan sukses
            return redirect()->route('kelahiran')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('kelahiran')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }
}
