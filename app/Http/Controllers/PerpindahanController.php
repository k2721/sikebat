<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Perpindahanpenduduk;


use Illuminate\Http\Request;

class PerpindahanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('perpindahan');
    }
    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_pindah' => 'required',
            'nik' => 'required',
            'no_kk' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required',
            'kecamatan' => 'required',
            'kota' => 'required'
        ]);

        $perpindahanpenduduk = Perpindahanpenduduk::create([
        'nik' => $request->nik,
        'no_kk' => $request->no_kk,
        'alamat' => $request->alamat,
        'rt' => $request->rt,
        'rw' => $request->rw,
        'kelurahan' => $request->kelurahan,
        'kecamatan' => $request->kecamatan,
        'kota' => $request->kota
        ]);

        if($perpindahanpenduduk){
        //redirect dengan pesan sukses
            return redirect()->route('perpindahan')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('perpindahan')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }
}
