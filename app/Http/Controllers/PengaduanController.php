<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengaduan;

class PengaduanController extends Controller
{
    //
    public function index(){
        //mengambil data dari tabel pegawai
        //$pengaduans = DB::table('pengaduan') ->get();

        //mengirim data pegawai ke view index

        
        $pengaduans = DB::table('tb_pengaduan');
        return view('user.pengaduan',compact('pengaduans'));
        

    }

    public function tambah(){
        
        return view('user.crud_pengaduan.tambah_data_pengaduan');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'nama' => 'required',
            'kritikSaran' => 'required'
           
        ]);

        $pengaduan = Pengaduan::create([
        'nama' => $request->nama,
        'kritikSaran' => $request->kritikSaran
        ]);

        if($pengaduan){
        //redirect dengan pesan sukses
            return redirect()->route('pengaduan')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('crud_pengaduan.tambah_data_pengaduan')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }


    
}

