<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Penduduk;

class CekController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $penduduks = DB::table('tb_penduduk')->where('nik', 'LIKE', '%' .$request->search.'%');
        }else{
            $penduduks = DB::table('tb_penduduk');
        }
        $penduduks = $penduduks->get();
        return view('cek', compact('penduduks'));
    }
    
}