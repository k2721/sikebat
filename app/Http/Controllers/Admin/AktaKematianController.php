<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aktakematian;
use App\Models\Jeniskelamin;
use App\Models\Agama;

class AktaKematianController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        if($request->has('search')){
            $aktakematians = DB::table('tb_akte_kematian')->where('nama', 'LIKE', '%' .$request->search.'%');
        }else{
            $aktakematians = DB::table('tb_akte_kematian');
        }
        $aktakematians = $aktakematians->get();

        return view('admin.aktakematian.aktakematian', compact('aktakematians', 'jeniskelamins', 'agamas'));
    }

    public function create()
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        return view('admin.aktakematian.createaktakematian', compact('jeniskelamins', 'agamas'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_akta_kematian' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_kematian' => 'required',
            'tanggal_kematian' => 'required',
            'id_jeniskelamin' => 'required',
            'anak_ke' => 'required',
            'id_agama' => 'required'
        ]);

        $aktakematian = Aktakematian::create([
        'id_akta_kematian' => $request->id_akta_kematian,
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'tempat_kematian' => $request->tempat_kematian,
        'tanggal_kematian' => $request->tanggal_kematian,
        'id_jeniskelamin' => $request->id_jeniskelamin,
        'anak_ke' => $request->anak_ke,
        'id_agama' => $request->id_agama
        ]);

        if($aktakematian){
        //redirect dengan pesan sukses
            return redirect()->route('admin.aktakematian')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createaktakematian')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($id_akta_kematian)
    {
        $aktakematians = Aktakematian::where('id_akta_kematian',$id_akta_kematian)->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        return view('admin.aktakematian.editaktakematian', compact('aktakematians', 'jeniskelamins', 'agamas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_akte_kematian')->where('id_akta_kematian',$request->id_akta_kematian)->update([
	    'id_akta_kematian' => $request->id_akta_kematian,
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'tempat_kematian' => $request->tempat_kematian,
        'tanggal_kematian' => $request->tanggal_kematian,
        'id_jeniskelamin' => $request->id_jeniskelamin,
        'anak_ke' => $request->anak_ke,
        'id_agama' => $request->id_agama
        ]);

        return redirect('/admin/aktakematian')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id_akta_kematian)
    {
        DB::table('tb_akte_kematian')->where('id_akta_kematian', $id_akta_kematian)->delete();

        return redirect ('/admin/aktakematian')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
