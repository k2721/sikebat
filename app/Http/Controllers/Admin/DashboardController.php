<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Penduduk;
use App\Models\Kartukeluarga;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $penduduks = DB::table('tb_penduduk')->get();
        $kartukeluargas = DB::table('tb_kk')->get();
        $lakis = DB::table('tb_penduduk')->where('id_jeniskelamin', '=', '1',$request->id_jeniskelamin)->get();
        $perempuans = DB::table('tb_penduduk')->where('id_jeniskelamin', '=', '2',$request->id_jeniskelamin)->get();
        $aktakelahirans = DB::table('tb_akta_kelahiran')->get();
        $aktakematians = DB::table('tb_akte_kematian')->get();
        $perpindahanpenduduks = DB::table('tb_pindah')->get();
        return view('admin.dashboard', compact('penduduks', 'kartukeluargas', 'lakis', 'perempuans', 'aktakelahirans', 'aktakematians', 'perpindahanpenduduks'));
    }
}
