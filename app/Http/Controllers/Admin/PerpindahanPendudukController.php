<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Perpindahanpenduduk;

class PerpindahanPendudukController extends Controller
{ 
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->has('search')){
            $perpindahanpenduduks = DB::table('tb_pindah')->where('nik', 'LIKE', '%' .$request->search.'%');
        }else{
            $perpindahanpenduduks = DB::table('tb_pindah');
        }
        $perpindahanpenduduks = $perpindahanpenduduks->get();

        return view('admin.perpindahanpenduduk.perpindahanpenduduk', compact('perpindahanpenduduks'));
    }

    public function create()
    {
        return view('admin.perpindahanpenduduk.createperpindahanpenduduk');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_pindah' => 'required',
            'nik' => 'required',
            'no_kk' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required',
            'kecamatan' => 'required',
            'kota' => 'required'
        ]);

        $perpindahanpenduduk = Perpindahanpenduduk::create([
        'id_pindah' => $request->id_pindah,
        'nik' => $request->nik,
        'no_kk' => $request->no_kk,
        'alamat' => $request->alamat,
        'rt' => $request->rt,
        'rw' => $request->rw,
        'kelurahan' => $request->kelurahan,
        'kecamatan' => $request->kecamatan,
        'kota' => $request->kota
        ]);

        if($perpindahanpenduduk){
        //redirect dengan pesan sukses
            return redirect()->route('admin.perpindahanpenduduk')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createperpindahanpenduduk')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($id_pindah)
    {
        $perpindahanpenduduks = Perpindahanpenduduk::where('id_pindah',$id_pindah)->get();

        return view('admin.perpindahanpenduduk.editperpindahanpenduduk', compact('perpindahanpenduduks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_pindah')->where('id_pindah',$request->id_pindah)->update([
	        'id_pindah' => $request->id_pindah,
	        'nik' => $request->nik,
	        'no_kk' => $request->no_kk,
	        'alamat' => $request->alamat,
	        'rt' => $request->rt,
	        'rw' => $request->rw,
	        'kelurahan' => $request->kelurahan,
	        'kecamatan' => $request->kecamatan,
	        'kota' => $request->kota
        ]);

        return redirect('/admin/perpindahanpenduduk')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id_pindah)
    {
        DB::table('tb_pindah')->where('id_pindah', $id_pindah)->delete();

        return redirect ('/admin/perpindahanpenduduk')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
