<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Penduduk;
use App\Models\Jeniskelamin;
use App\Models\Golongandarah;
use App\Models\Perkawinan;
use App\Models\Agama;
use App\Models\Kewarganegaraan;


class DataPendudukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $golongandarahs = DB::table('tb_golongandarah')->get();
        $perkawinans = DB::table('tb_perkawinan')->get();
        $agamas = DB::table('tb_agama')->get();

        if($request->has('search')){
            $penduduks = DB::table('tb_penduduk')->where('nama', 'LIKE', '%' .$request->search.'%');
        }else{
            $penduduks = DB::table('tb_penduduk');
        }
        $penduduks = $penduduks->get();

        return view('admin.datapenduduk.datapenduduk', compact('penduduks', 'jeniskelamins', 'golongandarahs', 'perkawinans', 'agamas'));
    }

    public function create()
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $kewarganegaraans = DB::table('tb_kewarganegaraan')->get();
        $golongandarahs = DB::table('tb_golongandarah')->get();
        $perkawinans = DB::table('tb_perkawinan')->get();
        $agamas = DB::table('tb_agama')->get();
        return view('admin.datapenduduk.createdatapenduduk', compact('jeniskelamins','kewarganegaraans', 'golongandarahs', 'perkawinans', 'agamas'));
    }

    public function store(Request $request)
    {

        $this->validate(request(), [
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'id_jeniskelamin' => 'required',
            'id_golongandarah' => 'required',
            'pekerjaan' => 'required',
            'pendidikan' => 'required',
            'id_perkawinan' => 'required',
            'id_kewarganegaraan' => 'required',
            'id_agama' => 'required',
        ]);

        $penduduk = Penduduk::create([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'id_jeniskelamin' => $request->id_jeniskelamin,
        'id_golongandarah' => $request->id_golongandarah,
        'pekerjaan' => $request->pekerjaan,
        'pendidikan' => $request->pendidikan,
        'id_perkawinan' => $request->id_perkawinan,
        'id_kewarganegaraan' => $request->id_kewarganegaraan,
        'id_agama' => $request->id_agama,
        'no_kk' => $request->no_kk
        ]);

        if($penduduk){
        //redirect dengan pesan sukses
            return redirect()->route('admin.datapenduduk')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createdatapenduduk')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($nik)
    {
        $penduduks = Penduduk::where('nik',$nik)->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $kewarganegaraans = DB::table('tb_kewarganegaraan')->get();
        $golongandarahs = DB::table('tb_golongandarah')->get();
        $perkawinans = DB::table('tb_perkawinan')->get();
        $agamas = DB::table('tb_agama')->get();

        return view('admin.datapenduduk.editdatapenduduk', compact('penduduks','kewar
        ganegaraans', 'jeniskelamins', 'golongandarahs', 'perkawinans', 'agamas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_penduduk')->where('nik',$request->nik)->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'id_jeniskelamin' => $request->id_jeniskelamin,
            'id_golongandarah' => $request->id_golongandarah,
            'pekerjaan' => $request->pekerjaan,
            'pendidikan' => $request->pendidikan,
            'id_perkawinan' => $request->id_perkawinan,
            'kewarganegaraan' => $request->kewarganegaraan,
            'id_agama' => $request->id_agama,
            'no_kk' => $request->no_kk
        ]);

        return redirect('/admin/datapenduduk')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($nik)
    {
        DB::table('tb_penduduk')->where('nik', $nik)->delete();

        return redirect ('/admin/datapenduduk')->with(['success' => 'Data Berhasil Didelete!']);
    }

}
