<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kartukeluarga;

class KartuKeluargaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->has('search')){
            $kartukeluargas = DB::table('tb_kk')->where('no_kk', 'LIKE', '%' .$request->search.'%');
        }else{
            $kartukeluargas = DB::table('tb_kk');
        }
        $kartukeluargas = $kartukeluargas->get();

        return view('admin.kartukeluarga.kartukeluarga', compact('kartukeluargas'));
    }

    public function create()
    {
        return view('admin.kartukeluarga.createkartukeluarga');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'no_kk' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'kelurahan' => 'required',
            'kota' => 'required',
            'kode_pos' => 'required',
            'provinsi' => 'required',
            'rt' => 'required',
            'rw' => 'required'
        ]);

        $kartukeluarga = Kartukeluarga::create([
        'no_kk' => $request->no_kk,
        'nik' => $request->nik,
        'alamat' => $request->alamat,
        'kelurahan' => $request->kelurahan,
        'kota' => $request->kota,
        'kode_pos' => $request->kode_pos,
        'provinsi' => $request->provinsi,
        'rt' => $request->rt,
        'rw' => $request->rw
        ]);

        if($kartukeluarga){
        //redirect dengan pesan sukses
            return redirect()->route('admin.kartukeluargas')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createkartukeluarga')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($no_kk)
    {
        $kartukeluargas = Kartukeluarga::where('no_kk',$no_kk)->get();

        return view('admin.kartukeluarga.editkartukeluarga', compact('kartukeluargas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_kk')->where('nik',$request->nik)->update([
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kota' => $request->kota,
            'kode_pos' => $request->kode_pos,
            'provinsi' => $request->provinsi,
            'rt' => $request->rt,
            'rw' => $request->rw
        ]);

        return redirect('/admin/kartukeluargas')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($no_kk)
    {
        DB::table('tb_kk')->where('no_kk', $no_kk)->delete();

        return redirect ('/admin/kartukeluargas')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
