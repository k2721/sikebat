<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengumuman;

class PengumumanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->has('search')){
            $pengumumans = DB::table('tb_pengumuman')->where('nama_kegiatan', 'LIKE', '%' .$request->search.'%');
        }else{
            $pengumumans = DB::table('tb_pengumuman');
        }
        $pengumumans = $pengumumans->get();

        return view('admin.pengumuman.pengumuman', compact('pengumumans'));
    }

    public function create()
    {
        return view('admin.pengumuman.createpengumuman');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_pengumuman' => 'required',
            'nama_kegiatan' => 'required',
            'tanggal_pelaksanaan' => 'required',
            'informasi_pelaksanaan' => 'required',
            'gambar' => 'required'
        ]);

        $pengumuman = Pengumuman::create([
        'id_pengumuman' => $request->id_pengumuman,
        'nama_kegiatan' => $request->nama_kegiatan,
        'tanggal_pelaksanaan' => $request->tanggal_pelaksanaan,
        'informasi_pelaksanaan' => $request->informasi_pelaksanaan,
        'gambar' => $request->gambar
        ]);

        if($pengumuman){
        //redirect dengan pesan sukses
            return redirect()->route('admin.pengumuman')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createpengumuman')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($id_pengumuman)
    {
        $pengumumans = Pengumuman::where('id_pengumuman',$id_pengumuman)->get();

        return view('admin.pengumuman.editpengumuman', compact('pengumumans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_pengumuman')->where('id_pengumuman',$request->id_pengumuman)->update([
            'nama_kegiatan' => $request->nama_kegiatan,
            'tanggal_pelaksanaan' => $request->tanggal_pelaksanaan,
            'informasi_pelaksanaan' => $request->informasi_pelaksanaan,
            'gambar' => $request->gambar
        ]);

        return redirect('/admin/pengumuman')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id_pengumuman)
    {
        DB::table('tb_pengumuman')->where('id_pengumuman', $id_pengumuman)->delete();

        return redirect ('/admin/pengumuman')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
