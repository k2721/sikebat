<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aktakelahiran;
use App\Models\Jeniskelamin;

class AktaKelahiranController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();

        if($request->has('search')){
            $aktakelahirans = DB::table('tb_akta_kelahiran')->where('nama_anak', 'LIKE', '%' .$request->search.'%');
        }else{
            $aktakelahirans = DB::table('tb_akta_kelahiran');
        }
        $aktakelahirans = $aktakelahirans->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        $q = DB::table('tb_akta_kelahiran')->select(DB::raw('MAX(RIGHT(id_akta_kelahiran,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('admin.aktakelahiran.aktakelahiran', compact('aktakelahirans', 'jeniskelamins', 'agamas', 'kd'));
    }

    public function create()
    {
        $aktakelahirans = DB::table('tb_akta_kelahiran')->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();
        $q = DB::table('tb_akta_kelahiran')->select(DB::raw('MAX(RIGHT(id_akta_kelahiran,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }
        return view('admin.aktakelahiran.createaktakelahiran', compact('aktakelahirans', 'jeniskelamins', 'agamas', 'kd'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_akta_kelahiran' => 'required',
            'nik' => 'required|min:16|max:17',
            'nama_anak' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'id_jeniskelamin' => 'required'
        ]);

        $aktakelahiran = Aktakelahiran::create([
        'id_akta_kelahiran' => $request->id_akta_kelahiran,
        'nik' => $request->nik,
        'nama_anak' => $request->nama_anak,
        'nama_ayah' => $request->nama_ayah,
        'nama_ibu' => $request->nama_ibu,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'id_jeniskelamin' => $request->id_jeniskelamin
        ]);

        if($aktakelahiran){
        //redirect dengan pesan sukses
            return redirect()->route('admin.aktakelahiran')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('admin.createaktakelahiran')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    public function edit($id_akta_kelahiran)
    {
        $aktakelahirans = Aktakelahiran::where('id_akta_kelahiran',$id_akta_kelahiran)->get();
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();

        return view('admin.aktakelahiran.editaktakelahiran', compact('aktakelahirans', 'jeniskelamins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_akta_kelahiran')->where('id_akta_kelahiran',$request->id_akta_kelahiran)->update([
	    'id_akta_kelahiran' => $request->id_akta_kelahiran,
        'nik' => $request->nik,
        'nama_anak' => $request->nama_anak,
        'nama_ayah' => $request->nama_ayah,
        'nama_ibu' => $request->nama_ibu,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'id_jeniskelamin' => $request->id_jeniskelamin
        ]);

        return redirect('/admin/aktakelahiran')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id_akta_kelahiran)
    {
        DB::table('tb_akta_kelahiran')->where('id_akta_kelahiran', $id_akta_kelahiran)->delete();

        return redirect ('/admin/aktakelahiran')->with(['success' => 'Data Berhasil Didelete!']);
    }
}
