<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Aktakematian;
use App\Models\Jeniskelamin;
use App\Models\Agama;

class KematianController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $jeniskelamins = DB::table('tb_jeniskelamin')->get();
        $agamas = DB::table('tb_agama')->get();

        return view('kematian', compact('jeniskelamins', 'agamas'));
    }
    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_akta_kematian' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_kematian' => 'required',
            'tanggal_kematian' => 'required',
            'jenis_kelamin' => 'required',
            'anak_ke' => 'required',
            'id_agama' => 'required'
        ]);

        $aktakematian = Aktakematian::create([
        'nik' => $request->nik,
        'nama' => $request->nama,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'tempat_kematian' => $request->tempat_kematian,
        'tanggal_kematian' => $request->tanggal_kematian,
        'jenis_kelamin' => $request->jenis_kelamin,
        'anak_ke' => $request->anak_ke,
        'id_agama' => $request->id_agama
        ]);

        if($aktakematian){
        //redirect dengan pesan sukses
            return redirect()->route('kematian')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('kematian')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }
}
