 <?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Penduduk extends Model
{
   public function euro()
   {
      return $this->belongsTo('App\Models\Jeniskelamin');
   }

}