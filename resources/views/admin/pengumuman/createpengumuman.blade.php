@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Tambah Data Pengumuman</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			<form action="{{ route('admin.storepengumuman') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>ID</label>
					<input type="text" name="id_pengumuman" class="form-control" placeholder="ID">
				</div>
				<div class="form-group">
					<label>Nama Kegiatan</label>
					<input type="text" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan">
				</div>
				<div class="form-group">
					<label>Tanggal Pelaksanaan</label>
					<input type="text" name="tanggal_pelaksanaan" class="form-control" placeholder="Tanggal Pelaksanaan">
				</div>
				<div class="form-group">
					<label>Informasi Pelaksanaan</label>
					<textarea id="ckview" name="informasi_pelaksanaan" class="form-control" rows="3" placeholder="Informasi Pelaksanaan"></textarea>
				</div>
				<div class="form-group">
					<label>Gambar</label>
					<input type="file" name="gambar" class="form-control">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			</div>
		</div>
	</div>
</div>

@endsection