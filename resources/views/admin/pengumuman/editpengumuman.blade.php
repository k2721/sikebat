@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Pengumuman</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($pengumumans as $pe)
			<form action="{{ route('admin.updatepengumuman') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id_pengumuman" value="{{ $pe->id_pengumuman }}">
				<div class="form-group">
					<label>Nama Kegiatan</label>
					<input type="text" name="nama_kegiatan" class="form-control" required="required" value="{{ $pe->nama_kegiatan }}">
				</div>
				<div class="form-group">
					<label>Tanggal Pelaksanaan</label>
					<input type="text" name="tanggal_pelaksanaan" class="form-control" required="required" value="{{ $pe->tanggal_pelaksanaan }}">
				</div>
				<div class="form-group">
					<label>Informasi Pelaksanaan</label>
					<textarea id="ckview" name="informasi_pelaksanaan" class="form-control" rows="3" required="required">{{ $pe->informasi_pelaksanaan }}</textarea>
				</div>
				<div class="form-group">
					<label>Gambar</label>
					<input type="file" name="gambar" class="form-control">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

@endsection