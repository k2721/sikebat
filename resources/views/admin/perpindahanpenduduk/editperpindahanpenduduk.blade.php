@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Perpindahan Penduduk</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($perpindahanpenduduks as $pp)
			<form action="{{ route('admin.updateperpindahanpenduduk') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id_pindah" value="{{ $pp->id_pindah }}">
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" required="required" value="{{ $pp->nik }}">
				</div>
				<div class="form-group">
					<label>No KK</label>
					<input type="text" name="no_kk" class="form-control" required="required" value="{{ $pp->no_kk}}">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" name="alamat" class="form-control" required="required" value="{{ $pp->alamat }}"></input>
				</div>
				<div class="form-group">
					<label>RT</label>
					<input type="text" name="rt" class="form-control" required="required" value="{{ $pp->rt }}">
				</div>
				<div class="form-group">
					<label>RW</label>
					<input type="text" name="rw" class="form-control" required="required" value="{{ $pp->rw }}">
				</div>
				<div class="form-group">
					<label>Kelurahan</label>
					<input type="text" name="kelurahan" class="form-control" required="required" value="{{ $pp->kelurahan }}">
				</div>
				<div class="form-group">
					<label>Kecamatan</label>
					<input type="text" name="kecamatan" class="form-control" required="required" value="{{ $pp->kecamatan }}">
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="kota" class="form-control" required="required" value="{{ $pp->kota }}">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

@endsection