@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Tambah Data Perpindahan Penduduk</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			<form action="{{ route('admin.storeperpindahanpenduduk') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>Id pindah</label>
					<input type="text" name="id_pindah" class="form-control" placeholder="Id Pindah">
				</div>
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>No KK</label>
					<input type="text" name="no_kk" class="form-control" placeholder="No KK">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" name="alamat" class="form-control" placeholder="Alamat"></input>
				</div>
				<div class="form-group">
					<label>RT</label>
					<input type="text" name="rt" class="form-control" placeholder="RT">
				</div>
				<div class="form-group">
					<label>RW</label>
					<input type="text" name="rw" class="form-control" placeholder="RW">
				</div>
				<div class="form-group">
					<label>Kelurahan</label>
					<input type="text" name="kelurahan" class="form-control" placeholder="Kelurahan">
				</div>
				<div class="form-group">
					<label>Kecamatan</label>
					<input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan">
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="kota" class="form-control" placeholder="Kota">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			</div>
		</div>
	</div>
</div>

@endsection