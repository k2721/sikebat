@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Penduduk</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($penduduks as $dp)
			<form action="{{ route('admin.updatedatapenduduk') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="nik" value="{{ $dp->nik }}">
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" required="required" value="{{ $dp->nama }}">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" required="required" value="{{ $dp->tempat_lahir }}">
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" required="required" value="{{ $dp->tanggal_lahir }}" />
				</div>
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="id_jeniskelamin" class="form-select" required>
	  					@foreach ($jeniskelamins as $jk)
	  						<option value="{{ $jk->id_jeniskelamin }}" {{ old('id_jeniskelamin', $dp->id_jeniskelamin) == $jk->id_jeniskelamin ? 'selected' : null}}>{{ $jk->jenis_kelamin }}</option>
	  					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Golongan Darah</label>
					<select name="id_golongandarah" class="form-select" required>
						@foreach ($golongandarahs as $gd)
	  						<option value="{{ $gd->id_golongandarah }}" {{ old('id_golongandarah', $dp->id_golongandarah) == $gd->id_golongandarah ? 'selected' : null}}>{{ $gd->golongan_darah }}</option>
	  					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Pekerjaan</label>
					<input type="text" name="pekerjaan" class="form-control" required="required" value="{{ $dp->pekerjaan }}">
				</div>
				<div class="form-group">
					<label>Pendidikan</label>
					<input type="text" name="pendidikan" class="form-control" required="required" value="{{ $dp->pendidikan}}">
				</div>
				<div class="form-group">
					<label>Status Perkawinan</label>
					<select name="id_perkawinan" class="form-select" required>
						@foreach ($perkawinans as $pn)
	  						<option value="{{ $pn->id_perkawinan }}" {{ old('id_perkawinan', $dp->id_perkawinan) == $pn->id_perkawinan ? 'selected' : null}}>{{ $pn->status_perkawinan }}</option>
	  					@endforeach
	  				</select>
				</div>
				<div class="form-group">
					<label>Kewarganegaraan</label>
					<select name="id_kewarganegaraan" class="form-select" required>
	  					<option value="" hidden>Kewarganegaraan</option>
	  					@foreach ($kewarganegaraans as $kw)
						  <option value="{{ $kw->id_kewarganegaraan }}" {{ old('id_kewarganegaraan', $kw->id_kewarganegaraan) == $kw->id_kewarganegaraan ? 'selected' : null}}>{{ $kw->kewarganegaraan }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>ID Agama</label>
					<select name="id_agama" class="form-select" required>
						@foreach ($agamas as $ag)
	  						<option value="{{ $ag->id_agama }}" {{ old('id_agama', $dp->id_agama) == $ag->id_agama ? 'selected' : null}}>{{ $ag->nama_agama }}</option>
	  					@endforeach
	  				</select>
				</div>
				
				<div class="form-group">
					<label>No. Kartu Keluarga</label>
					<input type="text" name="no_kk" class="form-control" required="required" value="{{ $dp->no_kk }}">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection