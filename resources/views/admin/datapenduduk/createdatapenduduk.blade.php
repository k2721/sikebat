@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Tambah Data Penduduk</center></h2>
			<br />
			<br />
			@include('layouts.messages')
			<br />
			<form action="{{ route('admin.storedatapenduduk') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" placeholder="Nama">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
				</div>
				 <div class="row form-group">
                	<label for="date">Tanggal Lahir</label>
                	<input id="datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" />
            	</div>
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="id_jeniskelamin" class="form-select" required>
	  					<option value="" hidden>Jenis Kelamin</option>
	  					@foreach ($jeniskelamins as $jk)
	 					<option value="{{ $jk->id_jeniskelamin }}">{{ $jk->jenis_kelamin }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Golongan Darah</label>
					<select name="id_golongandarah" class="form-select" required>
	  					<option value="" hidden>Golongan Darah</option>
	  					@foreach ($golongandarahs as $gd)
	 					<option value="{{ $gd->id_golongandarah }}">{{ $gd->golongan_darah }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Pekerjaan</label>
					<input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan">
				</div>
				<div class="form-group">
					<label>Pendidikan</label>
					<input type="text" name="pendidikan" class="form-control" placeholder="Pendidikan">
				</div>
				<div class="form-group">
					<label>Status Perkawinan</label>
					<select name="id_perkawinan" class="form-select" required>
	  					<option value="" hidden>Status Perkawinan</option>
	  					@foreach ($perkawinans as $pn)
	 					<option value="{{ $pn->id_perkawinan }}">{{ $pn->status_perkawinan }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Kewarganegaraan</label>
					<select name="id_kewarganegaraan" class="form-select" required>
	  					<option value="" hidden>Kewarganegaraan</option>
	  					@foreach ($kewarganegaraans as $kw)
	 					<option value="{{ $kw->id_kewarganegaraan }}">{{ $kw->kewarganegaraan }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Agama</label>
					<select name="id_agama" class="form-select" required>
	  					<option value="" hidden>Agama</option>
	  					@foreach ($agamas as $ag)
	 					<option value="{{ $ag->id_agama }}">{{ $ag->nama_agama }}</option>
	 					@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label>No. Kartu Keluarga</label>
					<input type="text" name="no_kk" class="form-control" placeholder="No. Kartu Keluarga">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			</div>
		</div>
	</div>
</div>

	<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection