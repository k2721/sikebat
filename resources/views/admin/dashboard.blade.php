@extends('layouts.admin.app')
@section('content')

<!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div>
                    <img src="{{URL::asset('img/Logo-Cimahi.png')}}" width="30" height="30" alt="homepage" />
                </div>
                <div class="sidebar-brand-text mx-3">Sikebat</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/admin/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/datapenduduk">
                    <i class="far fa-address-card"></i>
                    <span>Data Penduduk</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/kartukeluargas">
                    <i class="fas fa-home"></i>
                    <span>Kartu Keluarga</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/aktakelahiran">
                    <i class="fas fa-baby"></i>
                    <span>Akta Kelahiran</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/aktakematian">
                    <i class="fas fa-ambulance"></i>
                    <span>Akta Kematian</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/perpindahanpenduduk">
                    <i class="fas fa-globe-americas"></i>
                    <span>Perpindahan Penduduk</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/pengumuman">
                    <i class="fas fa-bullhorn"></i>
                    <span>Pengumuman</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto" >
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown" style="color:white;">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" style="color:black;" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre > 
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <br />
                <div class="container-fluid">


                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-primary shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-4">
                                                <h3>Jumlah Penduduk</h3></div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($penduduks) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-success shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-4">
                                                <h3>Kepala Keluarga</h3></div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($kartukeluargas) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-home fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-info shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Laki-Laki</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($lakis) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-male fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <br />
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Perempuan</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($perempuans) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-female fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Angka Kelahiran</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($aktakelahirans) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-baby fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-secondary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Angka Kematian</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($aktakematians) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-ambulance fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <br />
                    <div class="row">
                        <br />
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12"></div>

                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-dark shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-3"><h3>Perpindahan Penduduk</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($perpindahanpenduduks) }} Jiwa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-female fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            <!-- End of Main Content -->

            </div>
            <br />
            <br />

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
@endsection