@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Kartu Keluarga</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($kartukeluargas as $kk)
			<form action="{{ route('admin.updatekartukeluarga') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="no_kk" value="{{ $kk->no_kk }}">
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" required="required" value="{{ $kk->nik }}">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" name="alamat" class="form-control" required="required" value="{{ $kk->alamat }}">
				</div>
				<div class="form-group">
					<label>Kelurahan</label>
					<input type="text" name="kelurahan" class="form-control" required="required" value="{{ $kk->kelurahan }}"></input>
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="kota" class="form-control" required="required" value="{{ $kk->kota }}">
				</div>
				<div class="form-group">
					<label>Kode Pos</label>
					<input type="text" name="kode_pos" class="form-control" required="required" value="{{ $kk->kode_pos }}">
				</div>
				<div class="form-group">
					<label>Provinsi</label>
					<input type="text" name="provinsi" class="form-control" required="required" value="{{ $kk->provinsi }}">
				</div>
				<div class="form-group">
					<label>RT</label>
					<input type="text" name="rt" class="form-control" required="required" value="{{ $kk->rt }}">
				</div>
				<div class="form-group">
					<label>RW</label>
					<input type="text" name="rw" class="form-control" required="required" value="{{ $kk->rw }}">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

@endsection