@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Tambah Data Akta Kelahiran</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			<form action="{{ route('admin.storeaktakelahiran') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>Id Akta Kelahiran</label>
					<input type="text" name="id_akta_kelahiran" class="form-control" value="{{ 'AK-'.$kd }}" required readonly>
				</div>
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>Nama Anak</label>
					<input type="text" name="nama_anak" class="form-control" placeholder="Nama Anak">
				</div>
				<div class="form-group">
					<label>Nama Ayah</label>
					<input type="text" name="nama_ayah" class="form-control" placeholder="Nama Ayah"></input>
				</div>
				<div class="form-group">
					<label>Nama Ibu</label>
					<input type="text" name="nama_ibu" class="form-control" placeholder="Nama Ibu">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" />
				</div>
				<div class="form-group">
                        <label for="id_jeniskelamin">Jenis Kelamin :</label> <br>
                    <div class="form-check form-check-inline">
                        <label for="id_jeniskelamin">
                            <input type="radio" name="id_jeniskelamin" value="1">&nbsp;Laki-Laki</input>&nbsp; &nbsp;
                            <input type="radio" name="id_jeniskelamin" value="2">&nbsp;Perempuan</input>
                        </label>
                        </div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			</div>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection