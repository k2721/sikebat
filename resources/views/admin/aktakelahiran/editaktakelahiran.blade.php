@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Akta Kelahiran</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($aktakelahirans as $ak)
			<form action="{{ route('admin.updateaktakelahiran') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id_akta_kelahiran" value="{{ $ak->id_akta_kelahiran }}">
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" required="required" value="{{ $ak->nik }}">
				</div>
				<div class="form-group">
					<label>Nama Anak</label>
					<input type="text" name="nama_anak" class="form-control" required="required" value="{{ $ak->nama_anak}}">
				</div>
				<div class="form-group">
					<label>Nama Ayah</label>
					<input type="text" name="nama_ayah" class="form-control" required="required" value="{{ $ak->nama_ayah }}"></input>
				</div>
				<div class="form-group">
					<label>Nama Ibu</label>
					<input type="text" name="nama_ibu" class="form-control" required="required" value="{{ $ak->nama_ibu }}">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" required="required" value="{{ $ak->tempat_lahir }}">
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" required="required" value="{{ $ak->tanggal_lahir }}" />
				</div>
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="id_jeniskelamin" class="form-select" required>
	  					@foreach ($jeniskelamins as $jk)
	  						<option value="{{ $jk->id_jeniskelamin }}" {{ old('id_jeniskelamin', $ak->id_jeniskelamin) == $jk->id_jeniskelamin ? 'selected' : null}}>{{ $jk->jenis_kelamin }}</option>
	  					@endforeach
					</select>
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection