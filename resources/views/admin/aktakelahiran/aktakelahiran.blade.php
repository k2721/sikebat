@extends('layouts.admin.app')
@section('content')

<div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div>
                    <img src="{{URL::asset('img/Logo-Cimahi.png')}}" width="30" height="30" alt="homepage" />
                </div>
                <div class="sidebar-brand-text mx-3">Sikebat</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="/admin/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/datapenduduk">
                    <i class="far fa-address-card"></i>
                    <span>Data Penduduk</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/kartukeluargas">
                    <i class="fas fa-home"></i>
                    <span>Kartu Keluarga</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/admin/aktakelahiran">
                    <i class="fas fa-baby"></i>
                    <span>Akta Kelahiran</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/aktakematian">
                    <i class="fas fa-ambulance"></i>
                    <span>Akta Kematian</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/perpindahanpenduduk">
                    <i class="fas fa-globe-americas"></i>
                    <span>Perpindahan Penduduk</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/pengumuman">
                    <i class="fas fa-bullhorn"></i>
                    <span>Pengumuman</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-3">

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <h1 class="h3 mb-0 text-gray-800">Akta Kelahiran</h1>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto" >
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="dropdown" style="color:white;">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" style="color:black;" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre > 
                                    <b>{{ Auth::user()->name }}</b>
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>

                    </ul>

                </nav>
                <!-- End of Topbar -->
	
				<!-- Begin Page Content -->
                <div class="container-fluid">
                    @include('layouts.messages')
					<br/>
					<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a href="{{ route('admin.createaktakelahiran') }}">
						<button class="btn btn-success">+ Tambah Data</button>
					</a>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto"></ul>

                        <form action="/admin/aktakelahiran" method="GET">
						<div class="form-inline my-2 my-lg-0">
							<div class="input-group ps-5">
					  			<div id="search-autocomplete" class="form-outline">
					    			<input type="search" name="search" id="form1" class="form-control" placeholder="Cari Nama " />
					  			</div>
					  			<button type="submit" class="btn btn-primary">
					    			<i class="fas fa-search"></i>
					  			</button>
							</div>
						</div>
                        </form>
					</div>
				</nav>

					<br/>
					<br/>
					
					<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="table-dark">
						<tr>
							<th style="vertical-align: middle; text-align: center;"><font color="white">Id Akta Kelahiran</font></th>
							<th style="vertical-align: middle; text-align: center;"><font color="white">NIK</font></th>
							<th style="vertical-align: middle; text-align: center;"><font color="white">Nama Anak</font></th>
                            <th style="vertical-align: middle; text-align: center;"><font color="white">Nama Ayah</font></th>
                            <th style="vertical-align: middle; text-align: center;"><font color="white">Nama Ibu</font></th>
                            <th style="vertical-align: middle; text-align: center;"><font color="white">Tempat Lahir</font></th>
                            <th style="vertical-align: middle; text-align: center;"><font color="white">Tanggal Lahir</font></th>
                            <th style="vertical-align: middle; text-align: center;"><font color="white">Jenis Kelamin</font></th>
							<th style="vertical-align: middle; text-align: center;"><font color="white"><center>Action</center></font></th>
						</tr>
						</thead>

						<tbody>
                            @foreach ($aktakelahirans as $ak)
						<tr>
							<td style="vertical-align: middle; text-align: center;">{{ $ak->id_akta_kelahiran }}</td>
							<td style="vertical-align: middle; text-align: center;">{{ $ak->nik }}</td>
							<td style="vertical-align: middle; text-align: center;">{{ $ak->nama_anak }}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $ak->nama_ayah }}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $ak->nama_ibu }}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $ak->tempat_lahir }}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $ak->tanggal_lahir }}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $ak->id_jeniskelamin }}</td>
							<td>
                                <div class="container mb-0">
                                    <a href="/admin/aktakelahiran/edit/{{ $ak->id_akta_kelahiran }}" class="btn btn-success btn-block" role="button">Edit</a>
                                    <a href="/admin/aktakelahiran/{{ $ak->id_akta_kelahiran }}" class="btn btn-danger btn-block" role="button">Delete</a>
                                </div>
							</td>
						</tr>
                        @endforeach
						</tbody>
					</table>
					</div>
					</div>
					<br/>
					<br/>
					<br/>
				</div>

			<!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    	const searchFocus = document.getElementById('search-focus');
		const keys = [
		  { keyCode: 'AltLeft', isTriggered: false },
		  { keyCode: 'ControlLeft', isTriggered: false },
		];

		window.addEventListener('keydown', (e) => {
		  keys.forEach((obj) => {
		    if (obj.keyCode === e.code) {
		      obj.isTriggered = true;
		    }
		  });

		  const shortcutTriggered = keys.filter((obj) => obj.isTriggered).length === keys.length;

		  if (shortcutTriggered) {
		    searchFocus.focus();
		  }
		});

		window.addEventListener('keyup', (e) => {
		  keys.forEach((obj) => {
		    if (obj.keyCode === e.code) {
		      obj.isTriggered = false;
		    }
		  });
		});
    </script>
@endsection