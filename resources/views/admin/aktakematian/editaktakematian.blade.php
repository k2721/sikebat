@extends('layouts.admin.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Akta Kematian</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($aktakematians as $am)
			<form action="{{ route('admin.updateaktakematian') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id_akta_kematian" value="{{ $am->id_akta_kematian }}">
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" required="required" value="{{ $am->nik }}">
				</div>
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" required="required" value="{{ $am->nama}}">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" required="required" value="{{ $am->tempat_lahir }}"></input>
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" required="required" value="{{ $am->tanggal_lahir }}" />
				</div>
				<div class="form-group">
					<label>Tempat Kematian</label>
					<input type="text" name="tempat_kematian" class="form-control" required="required" value="{{ $am->tempat_kematian }}">
				</div>
				<div class="form-group">
					<label>Tanggal Kematian</label>
					<input id="datepicker1" name="tanggal_kematian" required="required" value="{{ $am->tanggal_kematian }}" />
				</div>
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="id_jeniskelamin" class="form-select" required>
	  					@foreach ($jeniskelamins as $jk)
	  						<option value="{{ $jk->id_jeniskelamin }}" {{ old('id_jeniskelamin', $am->id_jeniskelamin) == $jk->id_jeniskelamin ? 'selected' : null}}>{{ $jk->jenis_kelamin }}</option>
	  					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Anak Ke</label>
					<input type="text" name="anak_ke" class="form-control" required="required" value="{{ $am->anak_ke }}">
				</div>
				<div class="form-group">
					<label>Id Agama</label>
					<select name="id_agama" class="form-select" required>
						@foreach ($agamas as $ag)
	  						<option value="{{ $ag->id_agama }}" {{ old('id_agama', $am->id_agama) == $ag->id_agama ? 'selected' : null}}>{{ $ag->nama_agama }}</option>
	  					@endforeach
	  				</select>
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#datepicker1').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection