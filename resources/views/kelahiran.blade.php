@extends('layouts.app')
@section('content')
<center><h2>FORM AKTA KELAHIRAN</h2></center>
<br />
@include('layouts.messages')
<p>*Isi data dibawah dengan lengkap dan sesuai</p>
<form action="{{ route('storekelahiran') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>Id Akta Kelahiran</label>
					<input type="text" name="id_akta_kelahiran" class="form-control" value="{{ 'AK-'.$kd }}" required readonly>
				</div>
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>Nama Anak</label>
					<input type="text" name="nama_anak" class="form-control" placeholder="Nama Anak">
				</div>
				<div class="form-group">
					<label>Nama Ayah</label>
					<input type="text" name="nama_ayah" class="form-control" placeholder="Nama Ayah">
				</div>
				<div class="form-group">
					<label>Nama Ibu</label>
					<input type="text" name="nama_ibu" class="form-control" placeholder="Nama Ibu">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir"></input>
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" />
				</div>
				<div class="form-group">
                        <label for="id_jeniskelamin">Jenis Kelamin :</label> <br>
                    <div class="form-check form-check-inline">
                        <label for="id_jeniskelamin">
                            <input type="radio" name="id_jeniskelamin" value="1">&nbsp;Laki-Laki</input>&nbsp; &nbsp;
                            <input type="radio" name="id_jeniskelamin" value="2">&nbsp;Perempuan</input>
                        </label>
                        </div>
                </div>
				<div class="form-group">
					<label>Anak Ke</label>
					<input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke">
				</div>
				<div class="form-group">
					<label>Id Agama</label>
					<select name="id_agama" class="form-select" required>
	  					<option value="" hidden>Agama</option>
	  					@foreach ($agamas as $ag)
	 					<option value="{{ $ag->id_agama }}">{{ $ag->nama_agama }}</option>
	 					@endforeach
					</select>
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
      
      
<br />
<br />
<!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

    <script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#datepicker1').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>


@endsection