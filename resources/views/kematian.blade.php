@extends('layouts.app')
@section('content')
<center><h2>FORM KEMATIAN</h2></center>
<br />
@include('layouts.messages')
<p>*isi data dibawah dengan lengkap dan sesuai</p>
<form action="{{ route('storekematian') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>Id Akta Kematian</label>
					<input type="text" name="id_akta_kematian" class="form-control" placeholder="Id Akta Kematian">
				</div>
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" placeholder="Nama">
				</div>
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir"></input>
				</div>
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input id="datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" />
				</div>
				<div class="form-group">
					<label>Tempat Kematian</label>
					<input type="text" name="tempat_kematian" class="form-control" placeholder="Tempat Kematian">
				</div>
				<div class="form-group">
					<label>Tanggal Kematian</label>
					<input id="datepicker1" name="tanggal_kematian" placeholder="Tanggal Kematian" />
				</div>
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="id_jeniskelamin" class="form-select" required>
	  					<option value="" hidden>Jenis Kelamin</option>
	  					@foreach ($jeniskelamins as $jk)
	 					<option value="{{ $jk->id_jeniskelamin }}">{{ $jk->jenis_kelamin }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Anak Ke</label>
					<input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke">
				</div>
				<div class="form-group">
					<label>Id Agama</label>
					<select name="id_agama" class="form-select" required>
	  					<option value="" hidden>Agama</option>
	  					@foreach ($agamas as $ag)
	 					<option value="{{ $ag->id_agama }}">{{ $ag->nama_agama }}</option>
	 					@endforeach
					</select>
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#datepicker1').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection