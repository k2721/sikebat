@extends('layouts.app')
@section('content')
<center><h2>FORM KARTU KELUARGA</h2></center>
<br />
@include('layouts.messages')
<p>*Isi data dibawah dengan lengkap dan sesuai</p>
<div class="container">
<form action="{{ route('storekartukeluarga') }}" method="POST">
@csrf
				<div class="form-group">
					<label>No KK</label>
					<input type="text" name="no_kk" class="form-control" placeholder="No KK">
				</div>
				<div class="form-group">
					<label>NIK</label>
					<input type="text" name="nik" class="form-control" placeholder="NIK">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" name="alamat" class="form-control" placeholder="Alamat">
				</div>
				<div class="form-group">
					<label>Kelurahan</label>
					<input type="text" name="kelurahan" class="form-control" placeholder="Kelurahan"></input>
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="kota" class="form-control" placeholder="Kota">
				</div>
				<div class="form-group">
					<label>Kode Pos</label>
					<input type="text" name="kode_pos" class="form-control" placeholder="Kode Pos">
				</div>
				<div class="form-group">
					<label>Provinsi</label>
					<input type="text" name="provinsi" class="form-control" placeholder="Provinsi">
				</div>
				<div class="form-group">
					<label>RT</label>
					<input type="text" name="rt" class="form-control" placeholder="RT">
				</div>
				<div class="form-group">
					<label>RW</label>
					<input type="text" name="rw" class="form-control" placeholder="RW">
				</div>
				<br />
<center><button type="submit" class="btn btn-primary">Kirim</button></center>
			</form>
  

<br />
<br />
<!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

@endsection


