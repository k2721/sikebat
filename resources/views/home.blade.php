@extends('layouts.app')

 @section('content')
 <div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
     

      <!-- CAROUSEL -->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{asset('gambar/Banner Bantu Masyarakat Tahu COVID-19.jpeg')}}" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="{{asset('gambar/banner-web-corona.png')}}" class="d-block w-100" alt="...">
            <div class="carousel-item">
              <img src="{{asset('gambar/Logo-Cimahi.png')}}" class="d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </button>
        </div><br>  



        <div class="card">
          <div class="card-header">{{ __('Homepage') }}</div>

          <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
            @endif

            {{ __('selamat datang,') }}
            {{ Auth::user()->name }}
          </div>
        </div> <br>

        <!-- PRESENTASE -->
      
      
      <div class="row">
        <div class="col-lg-3 col-xs-6">

          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$penduduks->count()}}</h3>
              <p>JIWA</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
            <a href="#" class="small-box-footer">
              JUMLAH PENDUDUK 
            </a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">

          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$kartukeluargas->count()}}</h3>
              <p>JIWA</p>
            </div>
            <div class="icon">
              <i class="fas fa-home"></i>
            </div>
            <a href="#" class="small-box-footer">
              KEPALA KELUARGA 
            </a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">

          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$lakis->count()}}</h3>
              <p>JIWA</p>
            </div>
            <div class="icon">
              <i class="fas fa-male"></i>
            </div>
            <a href="#" class="small-box-footer">
              LAKI-LAKI 
            </a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">

          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$perempuans->count()}}</h3>
              <p>JIWA</p>
            </div>
            <div class="icon">
              <i class="fas fa-female"></i>
            </div>
            <a href="#" class="small-box-footer">
              PEREMPUAN
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">

          <div class="small-box bg-dark">
            <div class="inner">
              <h3>{{$penduduks->count()}}</h3>
              <p>JIWA</p>
            </div>
            <div class="icon">
              <i class="fas fa-baby"></i>
            </div>
            <a href="#" class="small-box-footer">
             ANGKA KELAHIRAN
           </a>
         </div>
       </div>
       <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-secondary">
          <div class="inner">
            <h3>{{$penduduks->count()}}<h3>
            <p>JIWA</p>
          </div>
          <div class="icon">
            <i class="fas fa-ambulance"></i>
          </div>
          <a href="#" class="small-box-footer">
           ANGKA KEMATIAN
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-xs-6">

      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{$penduduks->count()}}</h3>
          <p>JIWA</p>
        </div>
        <div class="icon">
          <i class="fas fa-city"></i>
        </div>
        <a href="#" class="small-box-footer">
         PERPINDAHAN PENDUDUK
       </a>
     </div>
   </div>

 </div>

      <!-- PENGUMUMAN -->
      <div class="card">
        <div class="card-header"><b>Pengumuman</b></div>
        @foreach ($pengumumans as $pe)
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">
            <div class="container">
              <div class="row">
                <div class="col-sm">
                  <center><img src= "{{url('img/'.$pe->gambar) }}" alt="" width="200" height="150"></center>
                </div>
                <div class="col-md-6">
                 <h5>{{ $pe->nama_kegiatan }}</h5> 
                 <p>{{ $pe->tanggal_pelaksanaan }}</p>
               </div>
               <div class="col-sm">
                
               </div>

             </div>
           </div>
         </div>
         @endforeach
       </div>
       <br />
       <br />
       <br />
       <br />
     </div>
     
   </div>
<!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
   @endsection
