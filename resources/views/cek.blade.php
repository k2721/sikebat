@extends('layouts.app')
@section('content')
<center><h2>FORM PENDATAAN PENDUDUK</h2></center>

<br />
<br />

<form>
  <center>
    <div class="form-group">
          <h5><b>Cari Data</b></h5>
          <input type="search" name="search" class="form-control" placeholder="Masukkan NIK Anda">
        </div>
  </center>
  <center><button type="submit" class="btn btn-primary">Cari</button></center>
  </form>

  <br />
  <br />
  <h5>Result : </h5>
  <div class="table-responsive">
          <table class="table table-bordered">
            <thead class="table-dark">
            <tr>
              <th style="vertical-align: middle; text-align: center;"><font color="white">NIK</font></th>
              <th style="vertical-align: middle; text-align: center;"><font color="white">Nama</font></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($penduduks as $dp)
            <tr>
              <td style="vertical-align: middle; text-align: center;">{{ $dp->nik }}</td>
              <td style="vertical-align: middle; text-align: center;">{{ $dp->nama }}</td>
            </tr>
                        @endforeach
            </tbody>
          </table>
          </div>

@endsection
