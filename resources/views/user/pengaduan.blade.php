@extends('layouts.app')
<style>
  .badan
{
    background-color: #2D3E40;
    padding: 13px 0;
    margin-top: 20px;
    color:white;
    border-radius:20px;
   
}
.peding-100
{
    padding: 120px;
}
 
.peding-30
{
    padding: 30px;
}
  </style>


 @section('content')
 
 @include('layouts.messages')
 
 <div class="container badan">
       
      <div class="row">
        <div class="col peding-100">
        <h1>Ajukan Saran / Kritik</h1>
        <p>
          Silahkan tinggalkan pesan anda, pada kolom yang tersedia.
        </p>
        </div>
 
        <div class="col peding-30">
            
        <form method="post" action="{{ route('user.storePengaduan') }}">
             @csrf
             <div class="form-group">
               <label for="">Nama Anda:</label>
               <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
             </div>
          
             <div class="form-group">
               <label for="">Kritik / Saran:</label>
               <textarea name="kritikSaran" class="form-control" cols="30" rows="7"></textarea>
             </div>
          
             <input class="btn btn-primary" type="submit" value="KIRIM">
          
           </form>
        </div>
      </div>
      </div>
            <br />
            <br />
            <br />
            <br />

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Kelurahan Cibabat, Cimahi Utara</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
   @endsection
