@extends('layouts.app')

<br>
<div class="container">
  <div class="row">
    <div class="col-sm">
      <center><img src= "{{asset('gambar/Logo-Cimahi.png')}}" alt="" width="75" height="80"></center>
    </div>
    <div class="col-sm-8">
     <h2>Website Sistem Informasi dan Pendataan Penduduk Cibabat</h2> 
   </div>
   <div class="col-sm">
    
   </div>
 </div>
 <br>


 @section('content')
 <div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
     

      <!-- CAROUSEL -->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{asset('gambar/Banner Bantu Masyarakat Tahu COVID-19.jpeg')}}" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="{{asset('gambar/banner-web-corona.png')}}" class="d-block w-100" alt="...">
            <div class="carousel-item">
              <img src="{{asset('gambar/Logo-Cimahi.png')}}" class="d-block w-100" alt="...">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </button>
        </div><br>  



        <div class="card">
          <div class="card-header">{{ __('Homepage') }}</div>

          <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
            @endif

            {{ __('selamat datang,') }}
            {{ Auth::user()->name }}
          </div>
        </div> <br>

        <!-- PRESENTASE -->
        <div class="row">
          <div class="col-sm-6">
           <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Jumlah Penduduk</h5>
              <p class="card-text">50 </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="card text-white bg-warning mb-3" style="max-width: 18rem;"> 
            <div class="card-body">
              <h5 class="card-title">Jumlah Keluarga</h5>
              <p class="card-text">20 </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="card text-white bg-primary mb-3" style="max-width: 18rem;"> 
            <div class="card-body">
              <h5 class="card-title">Laki laki</h5>
              <p class="card-text">25</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card text-white bg-danger mb-3" style="max-width: 18rem;"> 
            <div class="card-body">
              <h5 class="card-title">perempuan</h5>
              <p class="card-text">25</p>
            </div>
          </div>
        </div>


      </div>

      <!-- PENGUMUMAN -->
      <div class="card">
        <div class="card-header"><b>Pengumuman</b></div>
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">
            <div class="container">
              <div class="row">
                <div class="col-sm">
                  <center><img src= "{{asset('gambar/Logo-Cimahi.png')}}" alt="" width="75" height="80"></center>
                </div>
                <div class="col-sm-8">
                 <h5>Kegiatan Vaksiniasi</h5> 
                 <p>Rabu , 25 Maret 2022</p>
               </div>
               <div class="col-sm">
                
               </div>

             </div>
           </div>
         </div>
         


       </div>
     </div>
     
   </div>
   @endsection
