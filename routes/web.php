<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DataPendudukController;
use App\Http\Controllers\PendataanController;
use App\Http\Controllers\PendataanPendudukController;
use App\Http\Controllers\Admin\KartuKeluargaController;
use App\Http\Controllers\Admin\PengumumanController;
use App\Http\Controllers\PengaduanController;
use App\Http\Controllers\KematianController;
use App\Http\Controllers\PerpindahanController;
use App\Http\Controllers\CekController;
use App\Http\Controllers\Admin\PerpindahanPendudukController;
use App\Http\Controllers\Admin\AktaKelahiranController;
use App\Http\Controllers\Admin\AktaKematianController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/pendataan', [App\Http\Controllers\PendataanController::class, 'index'])->name('pendataan');
Route::get('/pendataanpenduduk', [App\Http\Controllers\PendataanPendudukController::class, 'index'])->name('pendataanpenduduk');
Route::post('/pendataanpenduduk/store', [App\Http\Controllers\PendataanPendudukController::class, 'store'])->name('storependataanpenduduk');
Route::get('/kartukeluarga', [App\Http\Controllers\KartuKeluargaController::class, 'index'])->name('kartukeluargas');
Route::post('/kartukeluarga/store', [App\Http\Controllers\KartuKeluargaController::class, 'store'])->name('storekartukeluarga');
Route::get('/kelahiran', [App\Http\Controllers\KelahiranController::class, 'index'])->name('kelahiran');
Route::get('/pengaduan', [App\Http\Controllers\PengaduanController::class, 'index'])->name('pengaduan');
Route::get('/pengaduan/create', [App\Http\Controllers\PengaduanController::class, 'tambah'])->name('user.tambahPengaduan');
Route::post('/pengaduan/store', [App\Http\Controllers\PengaduanController::class, 'store'])->name('user.storePengaduan');
Route::get('/kematian', [App\Http\Controllers\KematianController::class, 'index'])->name('kematian');
Route::get('/pindah', [App\Http\Controllers\PerpindahanController::class, 'index'])->name('perpindahan');
Route::get('/cek', [App\Http\Controllers\CekController::class, 'index'])->name('cek');
Route::post('kelahiran/store', [App\Http\Controllers\KelahiranController::class, 'store'])->name('storekelahiran');
Route::post('kematian/store', [App\Http\Controllers\KematianController::class, 'store'])->name('storekematian');
Route::post('pindah/store', [App\Http\Controllers\PerpindahanController::class, 'store'])->name('storeperpindahanpenduduk');
Route::get('/cek', [App\Http\Controllers\CekController::class, 'index'])->name('cek');

Route::prefix('admin')->group(function(){
    Route::get('/dashboard', [Admin\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/datapenduduk', [Admin\DataPendudukController::class, 'index'])->name('admin.datapenduduk');
    Route::get('/aktakelahiran', [Admin\AktaKelahiranController::class, 'index'])->name('admin.aktakelahiran');
    Route::get('/aktakematian', [Admin\AktaKematianController::class, 'index'])->name('admin.aktakematian');
    Route::get('/perpindahanpenduduk', [Admin\PerpindahanPendudukController::class, 'index'])->name('admin.perpindahanpenduduk');
    Route::get('/kartukeluargas', [Admin\KartuKeluargaController::class, 'index'])->name('admin.kartukeluargas');
    Route::get('/pengumuman', [Admin\PengumumanController::class, 'index'])->name('admin.pengumuman');

    Route::get('/datapenduduk/create', [Admin\DataPendudukController::class, 'create'])->name('admin.createdatapenduduk');
    Route::post('/datapenduduk/store', [Admin\DataPendudukController::class, 'store'])->name('admin.storedatapenduduk');
    Route::post('/datapenduduk/update', [Admin\DataPendudukController::class, 'update'])->name('admin.updatedatapenduduk');
    Route::get('/datapenduduk/edit/{nik}', [Admin\DataPendudukController::class, 'edit'])->name('admin.editdatapenduduk');
    Route::get('/datapenduduk/{nik}', [Admin\DataPendudukController::class, 'destroy'])->name('admin.destroydatapenduduk');

    Route::get('/kartukeluarga/create', [Admin\KartuKeluargaController::class, 'create'])->name('admin.createkartukeluarga');
    Route::post('/kartukeluarga/store', [Admin\KartuKeluargaController::class, 'store'])->name('admin.storekartukeluarga');
    Route::post('/kartukeluarga/update', [Admin\KartuKeluargaController::class, 'update'])->name('admin.updatekartukeluarga');
    Route::get('/kartukeluarga/edit/{no_kk}', [Admin\KartuKeluargaController::class, 'edit'])->name('admin.editkartukeluarga');
    Route::get('/kartukeluarga/{no_kk}', [Admin\KartuKeluargaController::class, 'destroy'])->name('admin.destroykartukeluarga');

    Route::get('/pengumuman/create', [Admin\PengumumanController::class, 'create'])->name('admin.createpengumuman');
    Route::post('/pengumuman/store', [Admin\PengumumanController::class, 'store'])->name('admin.storepengumuman');
    Route::post('/pengumuman/update', [Admin\PengumumanController::class, 'update'])->name('admin.updatepengumuman');
    Route::get('/pengumuman/edit/{id_pengumuman}', [Admin\PengumumanController::class, 'edit'])->name('admin.editpengumuman');
    Route::get('/pengumuman/{id_pengumuman}', [Admin\PengumumanController::class, 'destroy'])->name('admin.destroypengumuman');

    //ZAFAR
    Route::get('/perpindahanpenduduk/create', [Admin\PerpindahanPendudukController::class, 'create'])->name('admin.createperpindahanpenduduk');
    Route::post('/perpindahanpenduduk/store', [Admin\PerpindahanPendudukController::class, 'store'])->name('admin.storeperpindahanpenduduk');
    Route::post('/perpindahanpenduduk/update', [Admin\PerpindahanPendudukController::class, 'update'])->name('admin.updateperpindahanpenduduk');
    Route::get('/perpindahanpenduduk/edit/{id_pindah}', [Admin\PerpindahanPendudukController::class, 'edit'])->name('admin.editperpindahanpenduduk');
    Route::get('/perpindahanpenduduk/{id_pindah}', [Admin\PerpindahanPendudukController::class, 'destroy'])->name('admin.destroyperpindahanpenduduk');

    Route::get('/aktakelahiran/create', [Admin\AktaKelahiranController::class, 'create'])->name('admin.createaktakelahiran');
    Route::post('/aktakelahiran/store', [Admin\AktaKelahiranController::class, 'store'])->name('admin.storeaktakelahiran');
    Route::post('/aktakelahiran/update', [Admin\AktaKelahiranController::class, 'update'])->name('admin.updateaktakelahiran');
    Route::get('/aktakelahiran/edit/{id_akta_kelahiran}', [Admin\AktaKelahiranController::class, 'edit'])->name('admin.editaktakelahiran');
    Route::get('/aktakelahiran/{id_akta_kelahiran}', [Admin\AktaKelahiranController::class, 'destroy'])->name('admin.destroyaktakelahiran');

    Route::get('/aktakematian/create', [Admin\AktaKematianController::class, 'create'])->name('admin.createaktakematian');
    Route::post('/aktakematian/store', [Admin\AktaKematianController::class, 'store'])->name('admin.storeaktakematian');
    Route::post('/aktakematian/update', [Admin\AktaKematianController::class, 'update'])->name('admin.updateaktakematian');
    Route::get('/aktakematian/edit/{id_akta_kematian}', [Admin\AktaKematianController::class, 'edit'])->name('admin.editaktakematian');
    Route::get('/aktakematian/{id_akta_kematian}', [Admin\AktaKematianController::class, 'destroy'])->name('admin.destroyaktakematian');
    //ZAFAR


    //ADEL
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::get('/pendataan', [App\Http\Controllers\PendataanController::class, 'index'])->name('pendataan');
	Route::get('/pendataanpenduduk', [App\Http\Controllers\PendataanPendudukController::class, 'index'])->name('pendataanpenduduk');
	Route::get('/kartukeluarga', [App\Http\Controllers\KartuKeluargaController::class, 'index'])->name('kartukeluarga');
	Route::get('/kelahiran', [App\Http\Controllers\KelahiranController::class, 'index'])->name('kelahiran');
	Route::get('/pengaduan', [App\Http\Controllers\PengaduanController::class, 'index'])->name('pengaduan');
	Route::get('/kematian', [App\Http\Controllers\KematianController::class, 'index'])->name('kematian');
	Route::get('/pindah', [App\Http\Controllers\PerpindahanController::class, 'index'])->name('perpindahan');
	Route::get('/cek', [App\Http\Controllers\CekController::class, 'index'])->name('cek');
	Route::post('kelahiran/store', [App\Http\Controllers\KelahiranController::class, 'store'])->name('storekelahiran');
	Route::post('kematian/store', [App\Http\Controllers\KematianController::class, 'store'])->name('storekematian');
	Route::post('pindah/store', [App\Http\Controllers\PerpindahanController::class, 'store'])->name('storeperpindahanpenduduk');
	Route::get('/cek', [App\Http\Controllers\CekController::class, 'index'])->name('cek');
	//ADEL
});